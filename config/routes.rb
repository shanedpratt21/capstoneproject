Rails.application.routes.draw do
  

  get 'questions/submit'

  get 'questions/approve'

  get 'questions/reject'

  devise_for :users, :controllers => { :omniauth_callbacks => "callbacks" } , :path => 'u'
  get 'auth/:provider/callback', to: 'sessions#create'
  get 'auth/failure', to: redirect('/')
  get 'signout', to: 'sessions#destroy', as: 'signout'
  
  get 'new_game' => 'home#new_game'
  get 'current_games' => 'home#current_games'
  get 'past_games' => 'home#past_games'
  get 'settings' => 'home#settings'
  get 'stats' => 'home#stats'
  get 'trophies' => 'home#trophies'
  get 'question_submit' => 'questions#new'#'home#question_submit'
  
  get 'admin' => 'users#index'  #home#admin'
  get 'question_review' => 'questions#review'
  post 'question_approve' => 'questions#approve'
  get 'question_reject' => 'questions#reject'
  #get 'question_review_individual' => 'questions#show'

  
  get 'admin_save_user' => 'users#admin_save'
  get 'profile' => 'users#profile'
  #get 'users_controller/admin_save/:access_level', :as => :admin_save
  
  get 'overall_stats' => 'stats#overall'
  get 'overall_total_stats' => 'stats#overall_total'
  get 'overall_art_stats' => 'stats#overall_art'
  get 'overall_sports_stats' => 'stats#overall_sports'
  get 'overall_entertainment_stats' => 'stats#overall_entertainment'
  get 'overall_geography_stats' => 'stats#overall_geography'
  get 'overall_science_stats' => 'stats#overall_science'
  get 'overall_history_stats' => 'stats#overall_history'
  get 'monthly_stats' => 'stats#monthly'
  get 'monthly_total_stats' => 'stats#monthly_total'
  get 'monthly_art_stats' => 'stats#monthly_art'
  get 'monthly_sports_stats' => 'stats#monthly_sports'
  get 'monthly_entertainment_stats' => 'stats#monthly_entertainment'
  get 'monthly_geography_stats' => 'stats#monthly_geography'
  get 'monthly_science_stats' => 'stats#monthly_science'
  get 'monthly_history_stats' => 'stats#monthly_history'
  get 'weekly_stats' => 'stats#weekly'
  get 'weekly_total_stats' => 'stats#weekly_total'
  get 'weekly_art_stats' => 'stats#weekly_art'
  get 'weekly_sports_stats' => 'stats#weekly_sports'
  get 'weekly_entertainment_stats' => 'stats#weekly_entertainment'
  get 'weekly_geography_stats' => 'stats#weekly_geography'
  get 'weekly_science_stats' => 'stats#weekly_science'
  get 'weekly_history_stats' => 'stats#weekly_history'
  

  resources :sessions, only: [:create, :destroy]
  resources :home do
    get :tests, on: :collection
  
  end
  
  resources :games do
    get :question, on: :collection
    get :ajax_spinner, on: :collection
    get :timer, on: :collection
    get :rate_question_and_redirect_to_spinner, on: :collection
  end
  

  resources :questions  
  resources :games
  

  get 'resign' => 'games#resign'
  get 'category_spinner' => 'games#category_spinner'
  get 'games_show' => 'games#show'
  get 'games_create' => 'games#create'
  get 'games_question' => 'games#question'
  #get 'ajax_spinner' => 'games#ajax_spinner'
  get 'check_answer' => 'games#check_answer'
  get 'search' => 'home#new_game'
  get 'crown_category_choice' =>'games#crown_category_choice'
  get 'challenge' =>'challenges#challenge'
  get 'select_challenge_categories' => 'challenges#select_challenge_categories'
  get 'random_opponent' => 'games#random_opponent'
  get 'player_rate_question' => 'games#player_rate_question'
  get 'retry' => 'games#retry'
  get 'reset_powerup' => 'games#reset_powerup'
  
  devise_scope :user do 
    #root to: 'static_pages#home'
    match '/sessions/user', to: 'devise/sessions#create', via: :post
  end

  
  resources :users
  get 'edit_users' => 'users#edit'
  get 'modify_user' => 'users#modify_user'
  get 'achievements' => 'users#achievements'
  get 'user_stats' => 'users#stats'
  get 'update_settings' => 'users#update_settings'
  get 'checkout' => 'users#checkout'

  root to: "home#show"

  

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end

$(document).on('ready page:load', function() {

	var win_sound = document.getElementById('win'),
		fail_sound = document.getElementById('fail');

	if(win_sound) {
		win_sound.play();
	}
	if(fail_sound) {
		fail_sound.play();
	}
});
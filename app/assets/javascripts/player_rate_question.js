$(document).on('ready page:load', function() {
  
  function player_rate_question() {
    var data = $('.hidden_player_rating_data');
    var game_id = data.data('id');
    var questionId = data.data('question-id');
    var rating = $('#rating_selector').val();
    console.log(data);
    console.log(rating);
    console.log(questionId);
    console.log(game_id);
    
    
    $.ajax({
      url: '/games/rate_question_and_redirect_to_spinner',
      data: { id: game_id, question_id: questionId, rating: rating},
      type: 'get',
      //processData: false,
      //contentType:
      success: function(data) {
        //alert(data.text);
      }
    });
  }
  
  $('#player_rating_button').click(function() {
    player_rate_question();
  });
  
});
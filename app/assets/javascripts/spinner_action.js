$(document).on('ready page:load', function attachSpinnerHandler (){
  //$('#spinner_button').prop("disabled", false);
      console.log($('.hidden_sound_flag').data('soundFlag'));
      var items = ["Sports", "Entertainment", "Geography", "Science", "History", "Crown", "Art"],
          $text = $( '#spinner span' ),
          delay = .05, //seconds
          spin = Math.floor((Math.random() * 35) + 1),
          spinCounter = 0,
          gameId = $('.hidden_game_id').data('id'),
          spinner_sound = new Audio('spinner.mp3'),
          sound_flag = $('.hidden_sound_flag').data('soundFlag');
      
      function loop ( delay ) {
          $.each( items, function ( i, elm ){
              $text.delay( delay*1E3)/*.fadeOut()*/;
              $text.queue(function(){
                  $text.html( items[i] );
                  if( i == 0) {
                    $text.removeClass('art');
                    $text.addClass('sports');
                  }
                  if( i == 1) {
                    $text.removeClass('sports');
                    $text.addClass('entertainment');
                  }
                  if( i == 2) {
                    $text.removeClass('entertainment');
                    $text.addClass('geography');
                  }
                  if( i == 3) {
                    $text.removeClass('geography');
                    $text.addClass('science');
                  }
                  if( i == 4) {
                    $text.removeClass('science');
                    $text.addClass('history');
                  }
                  if( i == 5) {
                    $text.removeClass('history');
                    $text.addClass('crown');
                  }
                  if( i == 6) {
                    $text.removeClass('crown');
                    $text.addClass('art');
                  }
                  spinCounter++;
                  $text.dequeue();
              });
              //$text.fadeIn();
              $text.queue(function(){
                if (spinCounter >= spin) {
                    //$text.html(spinCounter);
                    spinner_sound.pause();
                    $.ajax({
                      url: '/games/ajax_spinner', 
                      data: {category_id: spinCounter + 1, id: gameId, category_question: false},
                      type: 'get',
                      success: function(data) {
                        //alert(data.text);
                      }
                    });
                    return;
                }
                else if ( i == items.length -1 ) {
                  delay += .05
                  loop(delay);   
                }
                $text.dequeue();
              });
          });
      }
      
      $('#spinner_button').click(function() {

        if(sound_flag) {
          spinner_sound.play();  
        }        
        loop( delay );
        $('#spinner_button').prop("disabled", true);
        
      });
});
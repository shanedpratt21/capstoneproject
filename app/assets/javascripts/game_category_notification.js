$(document).on('ready page:load', function() {
  $('#category_notification').click(function() {
    var message = $("#category_message").val();
    $.ajax({
      url: '/games/question', 
      data: {category: message},
      dataType: 'json',
      type: 'get',
      success: function(data) {
        alert(data.category.text);
      }
    });
  });
});
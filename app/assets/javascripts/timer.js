$(document).on('ready page:load', function() {
  
  var start = new Date();
  var maxTime = 30; //seconds
  var timeoutVal = 1000; //milliseconds
  var game_id = $('.hidden_timer_data').data('id');
  var answer_id = $('.hidden_timer_data').data('answer_id');
  var category_id = $('.hidden_timer_data').data('category_id');
  var crown_question = $('.hidden_timer_data').data('crown_question');
  var question_id = $('.hidden_timer_data').data('question_id');

  
  function updateProgress(time) {
      $('#timer').text(time);
      if(time == 0) {
        //ajax call
        
        $.ajax({
          url: '/games/timer',
          data: {id: game_id, 
            answer: answer_id,
            category: category_id,
            crown_question: crown_question,
            question_id: question_id
            /*answer_chosen: false*/ },
          type: 'get',
          success: function(data) {
            //alert(time);
          }
        });
        
      }
  }
  
  function animateUpdate() {
      var now = new Date();
      var timeDiff = Math.floor((now.getTime() - start.getTime())/1000);
      timeDiff = maxTime - timeDiff;
      console.log(timeDiff);
      if ((timeDiff >= 0) && document.getElementById('timer') && !(document.getElementById('freeze').value == 't' ) ) {
         updateProgress(timeDiff);
         setTimeout(animateUpdate, timeoutVal);
      }
  }
  
  if (document.getElementById('timer')) {

    animateUpdate();
  }
});
class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
		 :omniauthable, :omniauth_providers => [:facebook, :google_oauth2]
  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_initialize do |user|
      user.provider = auth.provider
      user.uid = auth.uid
      user.email = auth.info.email
      user.name = auth.info.name
      user.oauth_token = auth.credentials.token
      user.oauth_expires_at = Time.at(auth.credentials.expires_at)
      user.save
    end
  end
  def self.search(search)
    where("email like ? or name like ?", "%#{search}%", "%#{search}%")
  end

  #def paypal_url(return_url)
   # values = {
    #  :business => 'themphi-facilitator@my.westga.edu',
     #   :cmd => '_cart',
      #:upload => 1,
      #:return => return_url
    #} 
    # For test transactions use this url
    #{}"https://www.sandbox.paypal.com/cgi-bin/webscr?" + values.to_query
  #end

end

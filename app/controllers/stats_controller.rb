class StatsController < ApplicationController
  
  def overall
  end
  
  def overall_art
    @stats = PlayerStats.order(:total_correct)
    @perc_stats = Hash.new
  end
  
  def overall_sports
    @stats = PlayerStats.order(:total_correct)
    @perc_stats = Hash.new
  end
  
  def overall_entertainment
    @stats = PlayerStats.order(:total_correct)
    @perc_stats = Hash.new
  end
  
  def overall_geography
    @stats = PlayerStats.order(:total_correct)
    @perc_stats = Hash.new
  end
  
  def overall_science
    @stats = PlayerStats.order(:total_correct)
    @perc_stats = Hash.new
  end
  
  def overall_history
    @stats = PlayerStats.order(:total_correct)
    @perc_stats = Hash.new
  end
  
  def overall_total
    @stats = PlayerStats.order(:total_correct)
    @perc_stats = Hash.new
  end
  
  def monthly
  end
  
  def monthly_total
    @monthly_stats = MonthlyPlayerStats.where(created_at: 1.month.ago..DateTime.now)#.order(:created_at)
    @group = Hash.new
    @group_diff = Hash.new
    @group_perc = Hash.new
    User.find_each do |user|
      @group[user.id] = @monthly_stats.where(user_id: user.id)
      first_snapshot = @group[user.id].order(:created_at).first
      last_snapshot = @group[user.id].order(:created_at).last
      #byebug
      if !first_snapshot
        @group_diff[user.id] = 0
        @group_perc[user.id] = 0
      else
        @group_diff[user.id] = last_snapshot.total_correct - first_snapshot.total_correct
        if @group_diff[user.id] > 0
          @group_perc[user.id] = 100 * (@group_diff[user.id]).to_f / (last_snapshot.total_attempted - first_snapshot.total_attempted)
        else
          @group_perc[user.id] = 0
        end
      end
      
    end
    @group_diff.sort_by { |k,v| v }
    @group_perc.sort_by { |k,v| v }
  end
  
  def monthly_art
    @monthly_stats = MonthlyPlayerStats.where(created_at: 1.month.ago..DateTime.now)#.order(:created_at)
    @group = Hash.new
    @group_diff = Hash.new
    @group_perc = Hash.new
    User.find_each do |user|
      @group[user.id] = @monthly_stats.where(user_id: user.id)
      first_snapshot = @group[user.id].order(:created_at).first
      last_snapshot = @group[user.id].order(:created_at).last
      #byebug
      if !first_snapshot
        @group_diff[user.id] = 0
        @group_perc[user.id] = 0
      else
        @group_diff[user.id] = last_snapshot.art_correct - first_snapshot.art_correct
        if @group_diff[user.id] > 0
          @group_perc[user.id] = 100 * (@group_diff[user.id]).to_f / (last_snapshot.art_attempted - first_snapshot.art_attempted)
        else
          @group_perc[user.id] = 0
        end
      end
      
    end
    @group_diff.sort_by { |k,v| v }
    @group_perc.sort_by { |k,v| v }
  end
  
  def monthly_science
    @monthly_stats = MonthlyPlayerStats.where(created_at: 1.month.ago..DateTime.now)#.order(:created_at)
    @group = Hash.new
    @group_diff = Hash.new
    @group_perc = Hash.new
    User.find_each do |user|
      @group[user.id] = @monthly_stats.where(user_id: user.id)
      first_snapshot = @group[user.id].order(:created_at).first
      last_snapshot = @group[user.id].order(:created_at).last
      #byebug
      if !first_snapshot
        @group_diff[user.id] = 0
        @group_perc[user.id] = 0
      else
        @group_diff[user.id] = last_snapshot.science_correct - first_snapshot.science_correct
        if @group_diff[user.id] > 0
          @group_perc[user.id] = 100 * (@group_diff[user.id]).to_f / (last_snapshot.science_attempted - first_snapshot.science_attempted)
        else
          @group_perc[user.id] = 0
        end
      end
      
    end
    @group_diff.sort_by { |k,v| v }
    @group_perc.sort_by { |k,v| v }
  end
  
  def monthly_sports
    @monthly_stats = MonthlyPlayerStats.where(created_at: 1.month.ago..DateTime.now)#.order(:created_at)
    @group = Hash.new
    @group_diff = Hash.new
    @group_perc = Hash.new
    User.find_each do |user|
      @group[user.id] = @monthly_stats.where(user_id: user.id)
      first_snapshot = @group[user.id].order(:created_at).first
      last_snapshot = @group[user.id].order(:created_at).last
      #byebug
      if !first_snapshot
        @group_diff[user.id] = 0
        @group_perc[user.id] = 0
      else
        @group_diff[user.id] = last_snapshot.sports_correct - first_snapshot.sports_correct
        if @group_diff[user.id] > 0
          @group_perc[user.id] = 100 * (@group_diff[user.id]).to_f / (last_snapshot.sports_attempted - first_snapshot.sports_attempted)
        else
          @group_perc[user.id] = 0
        end
      end
      
    end
    @group_diff.sort_by { |k,v| v }
    @group_perc.sort_by { |k,v| v }
  end
  
  def monthly_history
    @monthly_stats = MonthlyPlayerStats.where(created_at: 1.month.ago..DateTime.now)#.order(:created_at)
    @group = Hash.new
    @group_diff = Hash.new
    @group_perc = Hash.new
    User.find_each do |user|
      @group[user.id] = @monthly_stats.where(user_id: user.id)
      first_snapshot = @group[user.id].order(:created_at).first
      last_snapshot = @group[user.id].order(:created_at).last
      #byebug
      if !first_snapshot
        @group_diff[user.id] = 0
        @group_perc[user.id] = 0
      else
        @group_diff[user.id] = last_snapshot.history_correct - first_snapshot.history_correct
        if @group_diff[user.id] > 0
          @group_perc[user.id] = 100 * (@group_diff[user.id]).to_f / (last_snapshot.history_attempted - first_snapshot.history_attempted)
        else
          @group_perc[user.id] = 0
        end
      end
      
    end
    @group_diff.sort_by { |k,v| v }
    @group_perc.sort_by { |k,v| v }
  end
  
  def monthly_entertainment
    @monthly_stats = MonthlyPlayerStats.where(created_at: 1.month.ago..DateTime.now)#.order(:created_at)
    @group = Hash.new
    @group_diff = Hash.new
    @group_perc = Hash.new
    User.find_each do |user|
      @group[user.id] = @monthly_stats.where(user_id: user.id)
      first_snapshot = @group[user.id].order(:created_at).first
      last_snapshot = @group[user.id].order(:created_at).last
      #byebug
      if !first_snapshot
        @group_diff[user.id] = 0
        @group_perc[user.id] = 0
      else
        @group_diff[user.id] = last_snapshot.entertainment_correct - first_snapshot.entertainment_correct
        if @group_diff[user.id] > 0
          @group_perc[user.id] = 100 * (@group_diff[user.id]).to_f / (last_snapshot.entertainment_attempted - first_snapshot.entertainment_attempted)
        else
          @group_perc[user.id] = 0
        end
      end
      
    end
    @group_diff.sort_by { |k,v| v }
    @group_perc.sort_by { |k,v| v }
  end
  
  def monthly_geography
    @monthly_stats = MonthlyPlayerStats.where(created_at: 1.month.ago..DateTime.now)#.order(:created_at)
    @group = Hash.new
    @group_diff = Hash.new
    @group_perc = Hash.new
    User.find_each do |user|
      @group[user.id] = @monthly_stats.where(user_id: user.id)
      first_snapshot = @group[user.id].order(:created_at).first
      last_snapshot = @group[user.id].order(:created_at).last
      #byebug
      if !first_snapshot
        @group_diff[user.id] = 0
        @group_perc[user.id] = 0
      else
        @group_diff[user.id] = last_snapshot.geography_correct - first_snapshot.geography_correct
        if @group_diff[user.id] > 0
          @group_perc[user.id] = 100 * (@group_diff[user.id]).to_f / (last_snapshot.geography_attempted - first_snapshot.geography_attempted)
        else
          @group_perc[user.id] = 0
        end
      end
      
    end
    @group_diff.sort_by { |k,v| v }
    @group_perc.sort_by { |k,v| v }
  end
  
  def weekly
  end
  
  def weekly_art
    @monthly_stats = MonthlyPlayerStats.where(created_at: 1.month.ago..DateTime.now)#.order(:created_at)
    @group = Hash.new
    @group_diff = Hash.new
    @group_perc = Hash.new
    User.find_each do |user|
      @group[user.id] = @monthly_stats.where(user_id: user.id)
      first_snapshot = @group[user.id].order(:created_at).first
      last_snapshot = @group[user.id].order(:created_at).last
      #byebug
      if !first_snapshot
        @group_diff[user.id] = 0
        @group_perc[user.id] = 0
      else
        @group_diff[user.id] = last_snapshot.art_correct - first_snapshot.art_correct
        if @group_diff[user.id] > 0
          @group_perc[user.id] = 100 * (@group_diff[user.id]).to_f / (last_snapshot.art_attempted - first_snapshot.art_attempted)
        else
          @group_perc[user.id] = 0
        end
      end
      
    end
    @group_diff.sort_by { |k,v| v }
    @group_perc.sort_by { |k,v| v }
  end
  
  def weekly_sports
    @monthly_stats = MonthlyPlayerStats.where(created_at: 1.month.ago..DateTime.now)#.order(:created_at)
    @group = Hash.new
    @group_diff = Hash.new
    @group_perc = Hash.new
    User.find_each do |user|
      @group[user.id] = @monthly_stats.where(user_id: user.id)
      first_snapshot = @group[user.id].order(:created_at).first
      last_snapshot = @group[user.id].order(:created_at).last
      #byebug
      if !first_snapshot
        @group_diff[user.id] = 0
        @group_perc[user.id] = 0
      else
        @group_diff[user.id] = last_snapshot.sports_correct - first_snapshot.sports_correct
        if @group_diff[user.id] > 0
          @group_perc[user.id] = 100 * (@group_diff[user.id]).to_f / (last_snapshot.sports_attempted - first_snapshot.sports_attempted)
        else
          @group_perc[user.id] = 0
        end
      end
      
    end
    @group_diff.sort_by { |k,v| v }
    @group_perc.sort_by { |k,v| v }
  end
  
  def weekly_science
    @monthly_stats = MonthlyPlayerStats.where(created_at: 1.month.ago..DateTime.now)#.order(:created_at)
    @group = Hash.new
    @group_diff = Hash.new
    @group_perc = Hash.new
    User.find_each do |user|
      @group[user.id] = @monthly_stats.where(user_id: user.id)
      first_snapshot = @group[user.id].order(:created_at).first
      last_snapshot = @group[user.id].order(:created_at).last
      #byebug
      if !first_snapshot
        @group_diff[user.id] = 0
        @group_perc[user.id] = 0
      else
        @group_diff[user.id] = last_snapshot.science_correct - first_snapshot.science_correct
        if @group_diff[user.id] > 0
          @group_perc[user.id] = 100 * (@group_diff[user.id]).to_f / (last_snapshot.science_attempted - first_snapshot.science_attempted)
        else
          @group_perc[user.id] = 0
        end
      end
      
    end
    @group_diff.sort_by { |k,v| v }
    @group_perc.sort_by { |k,v| v }
  end
  
  def weekly_history
    @monthly_stats = MonthlyPlayerStats.where(created_at: 1.month.ago..DateTime.now)#.order(:created_at)
    @group = Hash.new
    @group_diff = Hash.new
    @group_perc = Hash.new
    User.find_each do |user|
      @group[user.id] = @monthly_stats.where(user_id: user.id)
      first_snapshot = @group[user.id].order(:created_at).first
      last_snapshot = @group[user.id].order(:created_at).last
      #byebug
      if !first_snapshot
        @group_diff[user.id] = 0
        @group_perc[user.id] = 0
      else
        @group_diff[user.id] = last_snapshot.history_correct - first_snapshot.history_correct
        if @group_diff[user.id] > 0
          @group_perc[user.id] = 100 * (@group_diff[user.id]).to_f / (last_snapshot.history_attempted - first_snapshot.history_attempted)
        else
          @group_perc[user.id] = 0
        end
      end
      
    end
    @group_diff.sort_by { |k,v| v }
    @group_perc.sort_by { |k,v| v }
  end
  
  def weekly_entertainment
    @monthly_stats = MonthlyPlayerStats.where(created_at: 1.month.ago..DateTime.now)#.order(:created_at)
    @group = Hash.new
    @group_diff = Hash.new
    @group_perc = Hash.new
    User.find_each do |user|
      @group[user.id] = @monthly_stats.where(user_id: user.id)
      first_snapshot = @group[user.id].order(:created_at).first
      last_snapshot = @group[user.id].order(:created_at).last
      #byebug
      if !first_snapshot
        @group_diff[user.id] = 0
        @group_perc[user.id] = 0
      else
        @group_diff[user.id] = last_snapshot.entertainment_correct - first_snapshot.entertainment_correct
        if @group_diff[user.id] > 0
          @group_perc[user.id] = 100 * (@group_diff[user.id]).to_f / (last_snapshot.entertainment_attempted - first_snapshot.entertainment_attempted)
        else
          @group_perc[user.id] = 0
        end
      end
      
    end
    @group_diff.sort_by { |k,v| v }
    @group_perc.sort_by { |k,v| v }
  end
  
  def weekly_geography
    @monthly_stats = MonthlyPlayerStats.where(created_at: 1.month.ago..DateTime.now)#.order(:created_at)
    @group = Hash.new
    @group_diff = Hash.new
    @group_perc = Hash.new
    User.find_each do |user|
      @group[user.id] = @monthly_stats.where(user_id: user.id)
      first_snapshot = @group[user.id].order(:created_at).first
      last_snapshot = @group[user.id].order(:created_at).last
      #byebug
      if !first_snapshot
        @group_diff[user.id] = 0
        @group_perc[user.id] = 0
      else
        @group_diff[user.id] = last_snapshot.geography_correct - first_snapshot.geography_correct
        if @group_diff[user.id] > 0
          @group_perc[user.id] = 100 * (@group_diff[user.id]).to_f / (last_snapshot.geography_attempted - first_snapshot.geography_attempted)
        else
          @group_perc[user.id] = 0
        end
      end
      
    end
    @group_diff.sort_by { |k,v| v }
    @group_perc.sort_by { |k,v| v }
  end
  
  def weekly_total
    @monthly_stats = MonthlyPlayerStats.where(created_at: 1.week.ago..DateTime.now)#.order(:created_at)
    @group = Hash.new
    @group_diff = Hash.new
    @group_perc = Hash.new
    User.find_each do |user|
      @group[user.id] = @monthly_stats.where(user_id: user.id)
      first_snapshot = @group[user.id].order(:created_at).first
      last_snapshot = @group[user.id].order(:created_at).last
      #byebug
      if !first_snapshot
        @group_diff[user.id] = 0
        @group_perc[user.id] = 0
      else
        @group_diff[user.id] = last_snapshot.total_correct - first_snapshot.total_correct
        if @group_diff[user.id] > 0
          @group_perc[user.id] = 100 * (@group_diff[user.id]).to_f / (last_snapshot.total_attempted - first_snapshot.total_attempted)
        else
          @group_perc[user.id] = 0
        end
      end
      
    end
    @group_diff.sort_by { |k,v| v }
    @group_perc.sort_by { |k,v| v }
  end
  
end
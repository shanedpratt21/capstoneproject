class GamesController < ApplicationController

	def resign
		
		@game = Game.find(params[:game_id])
		stats = PlayerStats.where(user_id: current_user.id).first
		@game.update_attribute(:losingplayer_id, @game.currentplayer_id)
		if @game.currentplayer_id == @game.player1_id
			@game.update_attribute(:winningplayer_id, @game.player2_id)
		else
			@game.update_attribute(:winningplayer_id, @game.player1_id)
		end
		stats.update_attribute(:resigns, stats.resigns + 1)
		stats.update_attribute(:losses, stats.losses + 1)
		@game.update_attribute(:isfinished, true)
		redirect_to root_path
	end

	def new
		@game = Game.new
	end

	def random_opponent
		@player1 = current_user
		@user_count = User.count
		if @user_count < 2
			redirect_to new_game_path(alert: "No other players available.")
		else
			@opponents = User.where.not(:id => @player1.id)
			@player2 = @opponents.shuffle.first
			@game = Game.new(player1_id: @player1.id, player2_id: @player2.id, currentplayer_id: @player1.id)
			if @game.save
				@player1_score = Score.new
				@player1_score.game_id = @game.id
				@player1_score.user_id = @game.player1_id
				@player2_score = Score.new
				@player2_score.game_id = @game.id
				@player2_score.user_id = @game.player2_id
				@player1_score.save
				@player2_score.save
				redirect_to category_spinner_path(id: @game.id)
			end
		end
		
	end

	def category_spinner
		if params[:alert]
			@alert = params[:alert]
		end
		 
		@game = Game.find(params[:id])
		@challenge = Challenge.where(game_id: @game.id, challenged_id: current_user.id, challenge_pending: true).first
		@player1 = User.find(@game.player1_id)
		@player2 = User.find(@game.player2_id)

		player1_stats = PlayerStats.where(user_id: @player1.id).first
		player2_stats = PlayerStats.where(user_id: @player2.id).first
		@score = Score.where({:user_id => current_user.id, :game_id => @game.id}).first
		
		if current_user.id == @player1.id
			@score2 = Score.where({:user_id => @player2.id, :game_id => @game.id}).first
		else
			@score2 = Score.where({:user_id => @player1.id, :game_id => @game.id}).first
		end
		
		if @score.points == 6
			@game.winningplayer_id = @score.user_id
			if @game.player1_id == @score.user_id
				player1_stats.update_attribute(:wins, player1_stats.wins + 1)
				@game.losingplayer_id = @game.player2_id
			elsif @game.player2_id == @score.user_id
				player2_stats.update_attribute(:wins, player2_stats.wins + 1)
				@game.losingplayer_id = @game.player1_id
			end
			@game.isfinished = true
			@game.save
			redirect_to new_game_path
		end
		if params[:crown_progress] #where would this param come from?
    		@score.crown_progress = 0
    		@score.save

		end

		if @game.nil?
			@game = Game.new(game_params)
			@game.save
		end
	end

	def crown_category_choice
		@game = Game.find(params[:id])
		@score = Score.find(params[:score_id])
		@score.crown_progress = 0
		@score.save
		@categories = Array.new
		if @score.sports_category == false
			@categories.push(Category.find(2))
			@category_id = 2
		end
		if @score.art_category == false
			@categories.push(Category.find(1))
			@category_id = 1
		end
		if @score.entertainment_category == false
			@categories.push(Category.find(3))
			@category_id = 3
		end
		if @score.science_category == false
			@categories.push(Category.find(5))
			@category_id = 5
		end
		if @score.history_category == false
			@categories.push(Category.find(6))
			@category_id = 6
		end
		if @score.geography_category == false
			@categories.push(Category.find(4))
			@category_id = 4
		end
	end

	def retry
		@game = Game.find(params[:id])
		@score = Score.where({:user_id => current_user.id, :game_id => @game.id}).first
		@question = Question.find(params[:question_id])
		@category_id = params[:category_id]
		@crown_question = params[:crown_question]
		@score.retry = true
		@score.save
		current_user.coins -= 15
		current_user.save
		redirect_to games_question_path(id: @game.id, question_id: @question.id, category_id: @category_id, crown_question: @crown_question)
	end

	def reset_powerup
		@game = Game.find(params[:id])
		@score = Score.where({:user_id => current_user.id, :game_id => @game.id}).first
		@question = Question.find(params[:question_id])
		@category_id = params[:category_id]
		@crown_question = params[:crown_question]
		@score.save
		current_user.coins -= 5
		current_user.save
		redirect_to games_question_path(id: @game.id, question_id: @question.id, category_id: @category_id, crown_question: @crown_question)
	end



	def question
		
		if params[:challenge_question] == "true"
			@game = Game.find(params[:id])
			@challenge = Challenge.find(params[:challenge_id])
			@current_player_id = params[:current_player_id]
			@challenge_question = params[:challenge_question]
			@question_number = params[:question_number]
				if @question_number == "1"
					@question = Question.find(@challenge.art_question_id)
				elsif @question_number == "2"
					@question = Question.find(@challenge.sports_question_id)
				elsif @question_number == "3"
					@question = Question.find(@challenge.entertainment_question_id)
				elsif @question_number == "4"
					@question = Question.find(@challenge.geography_question_id)
				elsif @question_number == "5"
					@question = Question.find(@challenge.science_question_id)
				elsif @question_number == "6"
					@question = Question.find(@challenge.history_question_id)
				end
			@amount_correct = params[:amount_correct]
		else
			stats = PlayerStats.where(user_id: current_user.id).first
			@game = Game.find(params[:id])
			if params[:category_id]
				@category_id = (params[:category_id].to_i % 7)
				if(@category_id == 0)
					@category_id = 7
				end
				
			else
				@category_id = rand(1..7)
			end
			if @category_id == 7
				@score = Score.where({:user_id => @game.currentplayer_id, :game_id => @game.id}).first
				redirect_to crown_category_choice_path({id: @game.id, score_id: @score.id})
			else
				@crown_question = params[:crown_question]
			 	@category = Category.find(@category_id)
			 	@user = User.find(@game.currentplayer_id)
			 	if params[:question_id]
			 		@question = Question.find(params[:question_id])
			 	else
			 		@questions = Question.where(category_id: @category.id, approved: true, level: stats.level).all
			 		@question = @questions.shuffle.first
			 	end
			end			
		end
	end

	def get_piece(score_id, category_id)
		@score = Score.find(score_id)
		if category_id == "1"
			@score.art_category = true
		end
		if category_id == "2"
			@score.sports_category = true
		end
		if category_id == "3"
			@score.entertainment_category = true
		end
		if category_id == "4"
			@score.geography_category = true
		end
		if category_id == "5"
			@score.science_category = true
		end
		if category_id == "6"
			@score.history_category = true
		end
		@score.points += 1
		@score.save
		
	end

	def change_turns(game_id, answer_id, question_id)
		@game = Game.find(game_id)
		if @game.currentplayer_id == @game.player1_id
			@game.currentplayer_id = @game.player2_id
			@game.save
			redirect_to player_rate_question_path(id: @game.id, answer_id: answer_id, question_id: question_id)#category_spinner_path(id: @game.id)
		elsif @game.currentplayer_id == @game.player2_id
			@game.currentplayer_id = @game.player1_id
			@game.save
			redirect_to player_rate_question_path(id: @game.id, answer_id: answer_id, question_id: question_id)
		else
			redirect_to new_game_path	
		end
	end

	def check_answer
		if params[:freeze_coins]
			current_user.update_attribute(:coins, current_user.coins - params[:freeze_coins].to_i)
		end
		@game = Game.find(params[:id])
		
		@crown_question = params[:crown_question]
		@score = Score.where({:user_id => current_user.id, :game_id => @game.id}).first
		
		if current_user.id != @game.currentplayer_id
			redirect_to category_spinner_path(id: @game.id)
		end

		if params[:challenge_question] == "true"
			@challenge = Challenge.find(params[:challenge_id])
			@current_player_id = params[:current_player_id]
			@challenge_question = params[:challenge_question]
			@question_number = params[:question_number]
			@amount_correct = params[:amount_correct]
			@answer = Answer.find(params[:answer])
			if @answer.correct?
				@amount_correct = @amount_correct.to_i + 1
				current_user.coins += 2
				current_user.save
				@score.save
			end
			redirect_to challenge_path(game_id: @game.id, question_number: @question_number, amount_correct: @amount_correct, challenge_id: @challenge.id, challenge_question: @challenge_question)

		else
			
			@category_id = params[:category_id]
			increase_user_total_attempted
			increase_category_attempted(@category_id)
			if params[:answer]
				@question = Question.find(params[:question_id])
				@answer = Answer.find(params[:answer])
				#user = User.find(@game.currentplayer_id)
				if params[:crown_question] == "true"
					if @answer.correct?
						increase_user_total_correct
						increase_category_correct(@category_id)
						get_piece(@score.id, @category_id)
						current_user.coins += 2
						current_user.save
						if @score.save
							#redirect_to category_spinner_path(id: @game.id)

							redirect_to player_rate_question_path(id: @game.id, question_id: params[:question_id], answer_id: @answer.id)
						end
					else
						@score.save
						change_turns(@game.id, @answer.id, params[:question_id])
						#change_turns(@game.id)
					end
				else
					if @answer.correct?
						increase_user_total_correct
						increase_category_correct(@category_id)
						@score.crown_progress += 1
						current_user.coins += 2
						current_user.save
						if @score.save
							#redirect_to category_spinner_path(id: @game.id)
							redirect_to player_rate_question_path(id: @game.id, question_id: params[:question_id], answer_id: @answer.id)
						end
					else
						if @score.retry?
							@score.retry = false
							@score.save
							redirect_to games_question_path(id: @game.id, question_id:@question.id, category_id: @category_id, crown_question: false)
						else
							change_turns(@game.id, @answer.id, params[:question_id])
						end
						
					end
				end
			else
				if params[:crown_question] == "true"
					@score.crown_progress = 0
					@score.save
					change_turns(@game.id, @answer.id, params[:question_id])
				else
					change_turns(@game.id, nil, params[:question_id])
				end
			end
		end
	end
	
	def player_rate_question
		if params[:answer_id]
			@answer = Answer.find(params[:answer_id])
		else
			@answer = nil
		end
		@game_id = params[:id]
		@question_id = params[:question_id]
	end

	
	
	def increase_category_correct(category_id)
		stats = PlayerStats.where(user_id: current_user.id).first
		if category_id.to_i == 1
			stats.update_attribute(:art_correct, stats.art_correct + 1)
		elsif category_id.to_i == 2
			stats.update_attribute(:sports_correct, stats.sports_correct + 1)
		elsif category_id.to_i == 3
			stats.update_attribute(:entertainment_correct, stats.entertainment_correct + 1)
		elsif category_id.to_i == 4
			stats.update_attribute(:geography_correct, stats.geography_correct + 1)
		elsif category_id.to_i == 5
			stats.update_attribute(:science_correct, stats.science_correct + 1)
		elsif category_id.to_i == 6
			stats.update_attribute(:history_correct, stats.history_correct + 1)
		end
		create_player_stat_snapshot(stats) # make sure this is called so incorrect answers are stored as well
		check_category_achievement_unlocked(category_id)
	end
	
	def create_player_stat_snapshot(stats)
		snapshot = MonthlyPlayerStats.create
		snapshot.update_attribute(:user_id, stats.user_id)
		snapshot.update_attribute(:total_correct, stats.total_correct)
		snapshot.update_attribute(:total_attempted, stats.total_attempted)
		snapshot.update_attribute(:art_correct, stats.art_correct)
		snapshot.update_attribute(:art_attempted, stats.art_attempted)
		snapshot.update_attribute(:sports_correct, stats.sports_correct)
		snapshot.update_attribute(:sports_attempted, stats.sports_attempted)
		snapshot.update_attribute(:entertainment_correct, stats.entertainment_correct)
		snapshot.update_attribute(:entertainment_attempted, stats.entertainment_attempted)
		snapshot.update_attribute(:geography_correct, stats.geography_correct)
		snapshot.update_attribute(:geography_attempted, stats.geography_attempted)
		snapshot.update_attribute(:science_correct, stats.science_correct)
		snapshot.update_attribute(:science_attempted, stats.science_attempted)
		snapshot.update_attribute(:history_correct, stats.history_correct)
		snapshot.update_attribute(:history_attempted, stats.history_attempted)
		
	end
	
	def increase_category_attempted(category_id)
		stats = PlayerStats.where(user_id: current_user.id).first
		if category_id.to_i == 1
			stats.update_attribute(:art_attempted, stats.art_attempted + 1)
		elsif category_id.to_i == 2
			stats.update_attribute(:sports_attempted, stats.sports_attempted + 1)
		elsif category_id.to_i == 3
			stats.update_attribute(:entertainment_attempted, stats.entertainment_attempted + 1)
		elsif category_id.to_i == 4
			stats.update_attribute(:geography_attempted, stats.geography_attempted + 1)
		elsif category_id.to_i == 5
			stats.update_attribute(:science_attempted, stats.science_attempted + 1)
		elsif category_id.to_i == 6
			stats.update_attribute(:history_attempted, stats.history_attempted + 1)
		end
		create_player_stat_snapshot(stats)
	end
	
	def check_category_achievement_unlocked(category_id)
		stats = PlayerStats.where(user_id: current_user.id).first
		if category_id.to_i == 1 && stats.art_correct >= 10
			stats.update_attribute(:art_achievement, true)
		elsif category_id.to_i == 2 && stats.sports_correct >= 10
			stats.update_attribute(:sports_achievement, true)
		elsif category_id.to_i == 3 && stats.entertainment_correct >= 10
			stats.update_attribute(:entertainment_achievement, true)
		elsif category_id.to_i == 4 && stats.geography_correct >= 10
			stats.update_attribute(:geography_achievement, true)
		elsif category_id.to_i == 5 && stats.science_correct >= 10
			stats.update_attribute(:science_achievement, true)
		elsif category_id.to_i == 6 && stats.history_correct >= 10
			stats.update_attribute(:history_achievement, true)
		end
	end
	
	def increase_user_total_correct
		stats = PlayerStats.where(user_id: current_user.id).first
		stats.update_attribute(:total_correct, stats.total_correct + 1)
		if((stats.total_correct % 100 == 0) && (stats.level < 11))
			stats.update_attribute(:level, stats.level + 1)
		end
		check_total_achievement_unlocked
		create_player_stat_snapshot(stats)
	end
	
	def check_total_achievement_unlocked
		stats = PlayerStats.where(user_id: current_user.id).first
		if stats.total_correct >= 100
			stats.update_attribute(:total_achievement, true)
		end
	end
	
	def increase_user_total_attempted
		stats = PlayerStats.where(user_id: current_user.id).first
		stats.update_attribute(:total_attempted, stats.total_attempted + 1)
		create_player_stat_snapshot(stats)
	end

	def create
		@game = Game.new(game_params)
		if @game.save
			@player1_score = Score.new
			@player1_score.game_id = @game.id
			@player1_score.user_id = @game.player1_id
			@player2_score = Score.new
			@player2_score.game_id = @game.id
			@player2_score.user_id = @game.player2_id
			@player1_score.save
			@player2_score.save
			redirect_to category_spinner_path(id: @game.id)
		end

	end

	def show
		@game = Game.find(params[:id])
		redirect_to category_spinner_path(id: @game.id)
	end
	
	def rate_question_and_redirect_to_spinner
		rating = params[:rating]
		question_id = params[:question_id]
		#byebug
		question = Question.find(question_id)
		if((rating == 'Easy') && (question.level > 1))
			question.player_difficulty_votes -= 1
		elsif((rating == 'Hard') && (question.level < 10))
			question.player_difficulty_votes += 1
		end
		if (question.player_difficulty_votes == -10) && (question.level > 1) 
			question.level -= 1
			question.player_difficulty_votes = 0
		elsif (question.player_difficulty_votes == 10) && (question.level < 10)
			question.level += 1
			question.player_difficulty_votes = 0
		end
		question.save
		
		render :js => "window.location = '#{category_spinner_path(id: params[:id])}'"
	end
	
	def ajax_spinner
		cat = params[:category_id]
		gid = params[:id]
		#redirect_to(games_question_path(category_id: cat, id: gid, crown_question: false))
		render :js => "window.location = '#{games_question_path(category_id: cat, id: gid, crown_question: false)}'"
	end
	
	def timer
		game_id = params[:id]
		answer_id = params[:answer]
		#answer_chosen = params[:answer_chosen]
		crown = params[:crown_question]
		category_id = params[:category]
		render :js => "window.location = '#{check_answer_path(id: game_id, answer: answer_id,
			crown_question: crown, category_id: category_id)}'"
	end

	private

  def game_params
    params.require(:game).permit(:player1_id, :player2_id, :currentplayer_id)
  end

end

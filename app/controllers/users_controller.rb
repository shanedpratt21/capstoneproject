class UsersController < ApplicationController
  def new
    @user = User.new
    
  end

  def checkout
    current_user.update_attribute(:coins, current_user.coins + 5)
    redirect_to root_path
  end

  def show
    @user = User.find(params[:id])
  end

  def create
    
  end

  def edit
    @user = User.find(params[:format])
  end

  def update_settings
    @sound_flag = params[:sound_flag]
    current_user.sound_flag = @sound_flag
    current_user.save
    redirect_to settings_path
  end

  def update
    @user = User.find(params[:id])
    #byebug # check params[user]
    @user.update_attribute(:name, params[:user][:name])
    @user.update_attribute(:email, params[:user][:email])
    @user.update_attribute(:location, params[:user][:location])
    @user.update_attribute(:picture_privacy, params[:user][:picture_privacy])
    @user.update_attribute(:location_privacy, params[:user][:location_privacy])
    if params[:user][:password] == params[:user][:password_confirmation]
      @user.update_attribute(:password, params[:user][:password])
    end
    redirect_to root_path
    
  end
  
  def modify_user
    
    @user = User.find(params[:id])
    if(params[:access_level] == 'User')
      @user.update_attribute(:access_level, 0)
    elsif(params[:access_level] == 'Reviewer')
      @user.update_attribute(:access_level, 1)
    elsif(params[:access_level] == 'Admin')
      @user.update_attribute(:access_level, 2)
    else
      @user.update_attribute(:access_level, 3)
    end
    
    if @user.save
      redirect_to admin_path
    end
  end

  def index
  end

  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "User deleted"
    redirect_to user_path
  end
  
  def admin_save
    @user = User.find(params[:format])
    
    if(params[:access_level].to_str == 'User')
      @user.update_attribute(:access_level, 0)
    elsif(params[:access_level].to_str == 'Reviewer')
      @user.update_attribute(:access_level, 1)
    elsif(params[:access_level].to_str == 'Admin')
      @user.update_attribute(:access_level, 2)
    else
      @user.update_attribute(:access_level, 3)
    end
    if @user.save
      redirect_to admin_path
    end
  end
  
  def profile
    @user = User.find(params[:format])
    @stats = PlayerStats.where(user_id: params[:format]).first
    if @user.id != current_user.id
      @wins = Game.where(winningplayer_id: current_user.id, losingplayer_id: @user.id).length
      @losses = Game.where(winningplayer_id: @user.id, losingplayer_id: current_user.id).length
    end
    #byebug
  end
  
  def achievements
    @user = User.find(params[:format])
    @stats = PlayerStats.where(user_id: @user.id).first
    #byebug
  end
  
  def stats
    @user = User.find(params[:format])
    @stats = PlayerStats.where(user_id: @user.id).first
    
    if @stats.art_attempted > 0
      @art_avg = (@stats.art_correct.to_f / @stats.art_attempted) * 100
    else
      @art_avg = 0
    end
    if @stats.sports_attempted > 0
      @sport_avg = (@stats.sports_correct.to_f / @stats.sports_attempted) * 100
    else
      @sport_avg = 0
    end
    if @stats.entertainment_attempted > 0
      @ent_avg = (@stats.entertainment_correct.to_f / @stats.entertainment_attempted) * 100
    else
      @ent_avg = 0
    end
    if @stats.geography_attempted > 0
      @geo_avg = (@stats.geography_correct.to_f / @stats.geography_attempted) * 100
    else
      @geo_avg = 0
    end
    if @stats.science_attempted > 0
      @sci_avg = (@stats.science_correct.to_f / @stats.science_attempted) * 100
    else
      @sci_avg = 0
    end
    if @stats.history_attempted > 0
      @hist_avg = (@stats.history_correct.to_f / @stats.history_attempted) * 100
    else
      @hist_avg = 0
    end
    if @stats.total_attempted > 0
      @total_avg = (@stats.total_correct.to_f / @stats.total_attempted) * 100
    else
      @total_avg = 0
    end
  end
  
  private

    def user_params
      params.require(:user).permit(:name, :email, :password, :access_level,
                                   :password_confirmation)
    end
end

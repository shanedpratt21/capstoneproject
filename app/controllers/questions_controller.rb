class QuestionsController < ApplicationController
  def submit
  end

  def approve
    #params[:approved] = true
    @question = Question.find(cookies[:current_question])
    @question.update_attribute(:approved, true)
    @question.update_attribute(:level, params[:level].to_f)
    #byebug
    #@question.approved = true
    cookies.delete(:current_question)
    #@question.save
    redirect_to question_review_path
  end

  def reject
    redirect_to question_review_path
  end
  
  def show
    @question = Question.find(params[:id])
    @category = Category.find(@question.category_id)
    @answers = Array.new(4) { @question.answers.build  }
    #@question = 'question'
  end
  
  def edit
    @question = Question.find(params[:id])
    @answers = Array.new(4) { @question.answers.build  }
  end
  
  def destroy
    cookies.delete(:current_question)
    Question.find(params[:id]).destroy
    flash[:success] = "Question deleted"
    redirect_to question_review_path
  end
  
  def new
    @question = Question.new
    @answers = Array.new(4) { @question.answers.build  }
    #4.times { @question.answers.build }
  end
  
  def create
    @question = Question.new(question_params)
    if @question.save
      
      redirect_to root_path
    
    else
      render 'new'
    end
  end

  private

  def question_params
    params.require(:question).permit(:category_id, :username, :question, :approved, :level, answers_attributes: [:answer_text, :correct])
  end

  #def answer_params
   # params.require(:answer).permit({:question_id => [@question.id]}, :answer_text, :correct)
  #end

end

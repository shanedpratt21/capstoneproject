class ChallengesController < ApplicationController

	def new
		@challenge = Challenge.new
	end

	def select_challenge_categories
		@game = Game.find(params[:game_id])
		@challenging_player = User.find(params[:challenging_id])
		if @game.player1_id == @challenging_player.id
			@challenged_player = User.find(@game.player2_id)
		elsif @game.player2_id == @challenging_player.id
			@challenged_player = User.find(@game.player1_id)
		end
		@challenging_score = Score.where({:user_id => @challenging_player.id, :game_id => @game.id}).first
		@challenged_score = Score.where({:user_id => @challenged_player.id, :game_id => @game.id}).first

		@challenging_categories = Array.new
		if @challenging_score.art_category
			@challenging_categories.push(Category.find(1))
		end
		if @challenging_score.sports_category
			@challenging_categories.push(Category.find(2))
		end
		if @challenging_score.entertainment_category
			@challenging_categories.push(Category.find(3))
		end
		if @challenging_score.geography_category
			@challenging_categories.push(Category.find(4))
		end
		if @challenging_score.science_category
			@challenging_categories.push(Category.find(5))
		end
		if @challenging_score.history_category
			@challenging_categories.push(Category.find(6))
		end
		

		@challenged_categories = Array.new
		if @challenged_score.art_category && @challenging_score.art_category == false
			@challenged_categories.push(Category.find(1))
		end
		if @challenged_score.sports_category && @challenging_score.sports_category == false
			@challenged_categories.push(Category.find(2))
		end
		if @challenged_score.entertainment_category && @challenging_score.entertainment_category == false
			@challenged_categories.push(Category.find(3))
		end
		if @challenged_score.geography_category && @challenging_score.geography_category == false
			@challenged_categories.push(Category.find(4))
		end
		if @challenged_score.science_category && @challenging_score.science_category == false
			@challenged_categories.push(Category.find(5))
		end
		if @challenged_score.history_category && @challenging_score.history_category == false
			@challenged_categories.push(Category.find(6))
		end

		if @challenging_categories.empty?
			redirect_to category_spinner_path(alert: "You don't have any categories to risk", id: @game.id)
		else
			if @challenged_categories.empty?
				redirect_to category_spinner_path(alert: "No categories to steal from opponent", id: @game.id)
			end
		end
	end

	def challenge
		@game = Game.find(params[:game_id])
		if params[:challenge_question] == "true"
			@challenge = Challenge.find(params[:challenge_id])
			@amount_correct = params[:amount_correct]
			if params[:question_number] == "1"
				redirect_to games_question_path(id: @game.id, challenge_id: @challenge.id, question_id: @challenge.sports_question_id, current_player_id: current_user.id, challenge_question: true, question_number: 2, amount_correct: @amount_correct)
			end
			if params[:question_number] == "2"
				redirect_to games_question_path(id: @game.id, challenge_id: @challenge.id, question_id: @challenge.entertainment_question_id, current_player_id: current_user.id, challenge_question: true, question_number: 3, amount_correct: @amount_correct)
			end
			if params[:question_number] == "3"
				redirect_to games_question_path(id: @game.id, challenge_id: @challenge.id, question_id: @challenge.geography_question_id, current_player_id: current_user.id, challenge_question: true, question_number: 4, amount_correct: @amount_correct)
			end
			if params[:question_number] == "4"
				redirect_to games_question_path(id: @game.id, challenge_id: @challenge.id, question_id: @challenge.science_question_id, current_player_id: current_user.id, challenge_question: true, question_number: 5, amount_correct: @amount_correct)
			end
			if params[:question_number] == "5"
				redirect_to games_question_path(id: @game.id, challenge_id: @challenge.id, question_id: @challenge.history_question_id, current_player_id: current_user.id, challenge_question: true, question_number: 6, amount_correct: @amount_correct)
			end
			if params[:question_number] == "6"
				if current_user.id == @challenge.challenging_id 
					@challenge.challenging_correct = @amount_correct
					@challenge.challenge_pending = true
					@challenge.save
					change_turns(@game.id)
				elsif current_user.id == @challenge.challenged_id
					@challenge.challenged_correct = @amount_correct
					@challenge.challenge_pending = false
					@challenge.save
					get_winner(@challenge.id, @game.id)
				end
				
			end
		else
			create_challenge(@game.id)
		end
			# redirect_to question_path(question_id: @challenge.art_question_id, challenge_question: true, question_number: 1)
			
	end

	def get_winner(challenge_id, game_id)
		@game = Game.find(game_id)
		@challenge = Challenge.find(challenge_id)
		@challenging_player = User.find(@challenge.challenging_id)
		@challenged_player = User.find(@challenge.challenged_id)
		@challenging_score = Score.where({:user_id => @challenging_player.id, :game_id => @game.id}).first
		@challenged_score = Score.where({:user_id => @challenged_player.id, :game_id => @game.id}).first

		if @challenge.challenging_correct > @challenge.challenged_correct
			if @challenge.challenged_category == 1
				@challenging_score.art_category = true
				@challenging_score.points += 1
				@challenged_score.art_category = false
				@challenged_score.points -= 1
				@challenging_score.save
				@challenged_score.save
				change_turns(@game.id)
			end
			if @challenge.challenged_category == 2
				@challenging_score.sports_category = true
				@challenging_score.points += 1
				@challenged_score.sports_category = false
				@challenged_score.points -= 1
				@challenging_score.save
				@challenged_score.save
				change_turns(@game.id)
			end
			if @challenge.challenged_category == 3
				@challenging_score.entertainment_category = true
				@challenging_score.points += 1
				@challenged_score.entertainment_category = false
				@challenged_score.points -= 1
				@challenging_score.save
				@challenged_score.save
				change_turns(@game.id)
			end
			if @challenge.challenged_category == 4
				@challenging_score.geography_category = true
				@challenging_score.points += 1
				@challenged_score.geography_category = false
				@challenged_score.points -= 1
				@challenging_score.save
				@challenged_score.save
				change_turns(@game.id)
			end
			if @challenge.challenged_category == 5
				@challenging_score.science_category = true
				@challenging_score.points += 1
				@challenged_score.science_category = false
				@challenged_score.points -= 1
				@challenging_score.save
				@challenged_score.save
				change_turns(@game.id)
			end
			if @challenge.challenged_category == 6
				@challenging_score.history_category = true
				@challenging_score.points += 1
				@challenged_score.history_category = false
				@challenged_score.points -= 1
				@challenging_score.save
				@challenged_score.save
				change_turns(@game.id)
			end
		else
			if @challenge.challenging_category == 1
				@challenging_score.art_category = false
				@challenging_score.points -= 1
				@challenging_score.save
				Challenge.find(@challenge.id).destroy
				redirect_to category_spinner_path(@game.id)
			end
			if @challenge.challenging_category == 2
				@challenging_score.sports_category = false
				@challenging_score.points -= 1
				@challenging_score.save
				Challenge.find(@challenge.id).destroy
				redirect_to category_spinner_path(@game.id)
			end
			if @challenge.challenging_category == 3
				@challenging_score.entertainment_category = false
				@challenging_score.points -= 1
				@challenging_score.save
				Challenge.find(@challenge.id).destroy
				redirect_to category_spinner_path(@game.id)
			end
			if @challenge.challenging_category == 4
				@challenging_score.geography_category = false
				@challenging_score.points -= 1
				@challenging_score.save
				Challenge.find(@challenge.id).destroy
				redirect_to category_spinner_path(@game.id)
			end
			if @challenge.challenging_category == 5
				@challenging_score.science_category = false
				@challenging_score.points -= 1
				@challenging_score.save
				Challenge.find(@challenge.id).destroy
				redirect_to category_spinner_path(@game.id)
			end
			if @challenge.challenging_category == 6
				@challenging_score.history_category = false
				@challenging_score.points -= 1
				@challenging_score.save
				Challenge.find(@challenge.id).destroy
				redirect_to category_spinner_path(@game.id)
			end
		end
	end

	def change_turns(game_id)
		@game = Game.find(game_id)
		if @game.currentplayer_id == @game.player1_id
			@game.currentplayer_id = @game.player2_id
			@game.save
			redirect_to category_spinner_path(id: @game.id)
		elsif @game.currentplayer_id == @game.player2_id
			@game.currentplayer_id = @game.player1_id
			@game.save
			redirect_to category_spinner_path(id: @game.id)
		else
			redirect_to new_game_path	
		end
	end

	def create_challenge(game_id)
		@game = Game.find(game_id) 
		@challenge = Challenge.new
		@challenge.game_id = params[:game_id]
		@challenge.challenging_id = params[:challenging_id]
		@challenge.challenged_id = params[:challenged_id]
		@challenge.challenging_category = params[:challenging_category]
		@challenge.challenged_category = params[:challenged_category]
		# @challenging_player = User.find(params[:challenging_id])
		# @challenged_player = User.find(params[:challenged_id])
		if @challenge.save
			@art_question = Question.where({:category_id => 1}).all.shuffle.first
			@sports_question = Question.where({:category_id => 2}).all.shuffle.first
			@entertainment_question = Question.where({:category_id => 3}).all.shuffle.first
			@geography_question = Question.where({:category_id => 4}).all.shuffle.first
			@science_question = Question.where({:category_id => 5}).all.shuffle.first
			@history_question = Question.where({:category_id => 6}).all.shuffle.first
			@challenge.art_question_id = @art_question.id
			@challenge.sports_question_id = @sports_question.id
			@challenge.entertainment_question_id = @entertainment_question.id
			@challenge.geography_question_id = @geography_question.id
			@challenge.science_question_id = @science_question.id
			@challenge.history_question_id = @history_question.id
			@challenge.save	
		end	
		@score = Score.where({:user_id => current_user.id, :game_id => @game.id}).first
		@score.crown_progress = 0
		@score.save
		redirect_to games_question_path(id: @game.id, question_id: @challenge.art_question_id, current_player_id: current_user.id, challenge_id: @challenge.id, challenge_question: true, question_number: 1, amount_correct: 0)
	end

private
	def challenge_params
    	params.require(:challenge).permit(:game_id, :challenging_id, :challenged_id, :challenging_category, :challenged_category)
    end
end

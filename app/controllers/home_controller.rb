class HomeController < ApplicationController
  def show
    @questions = Question.all
    @stats = PlayerStats.where(:user_id => current_user.id)
    if @stats.empty?
      @stats = PlayerStats.new
      @stats.update_attribute(:user_id, current_user.id)
    end
    
  end
  def new_game
    if params[:alert]
      @alert = params[:alert]
    end
    if params[:search]
      @users = User.search(params[:search])
    else
      @users = Array.new
    end
  end
  def current_games
  end
  def past_games
  end
  def settings
    @sound_flag = current_user.sound_flag
  end
  def stats
  end
  def trophies
  end
  def question_submit
  end
  def question_review
  end
  def admin
  end
  def tests
    data = params[:test_data]
    respond_to do |format|
      format.json{
        render json: {
          text: data
        }
      }
    end
  end
end

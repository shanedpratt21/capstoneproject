class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :authenticate_user!
  #before_filter :require_user
  helper_method :current_user

  #def require_user
   # redirect_to new_user_session_path unless current_user
  #end
  def after_sign_in_path_for(resource)
      root_path
  end
end

**Rails Installer**

http://railsinstaller.org/en

**Google authentication**

http://richonrails.com/articles/google-authentication-in-ruby-on-rails

**Missing Permission**

https://raw.githubusercontent.com/rubygems/rubygems/master/lib/rubygems/ssl_certs/AddTrustExternalCARoot-2048.pem

**Managing Multiple Providers**

https://github.com/intridea/omniauth/wiki/Managing-Multiple-Providers

**SSL error fix**

https://gist.github.com/fnichol/867550


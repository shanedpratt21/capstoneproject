class CreateMonthlyPlayerStats < ActiveRecord::Migration
  def change
    create_table :monthly_player_stats do |t|
      t.integer :user_id
      t.integer :total_correct
      t.integer :total_attempted
      t.integer :art_correct
      t.integer :art_attempted
      t.integer :sports_correct
      t.integer :sports_attempted
      t.integer :entertainment_correct
      t.integer :entertainment_attempted
      t.integer :geography_correct
      t.integer :geography_attempted
      t.integer :science_correct
      t.integer :science_attempted
      t.integer :history_correct
      t.integer :history_attempted
      t.timestamps
    end
  end
end

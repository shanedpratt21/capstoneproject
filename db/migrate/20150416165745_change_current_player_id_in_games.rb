class ChangeCurrentPlayerIdInGames < ActiveRecord::Migration
  def change
    change_column :games, :currentplayer_id, :integer
  end
end

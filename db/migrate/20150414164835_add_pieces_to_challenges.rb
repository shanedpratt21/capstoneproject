class AddPiecesToChallenges < ActiveRecord::Migration
  def change
    add_column :challenges, :challenging_category, :integer
    add_column :challenges, :challenged_category, :integer
  end
end

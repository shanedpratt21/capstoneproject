class CreateGames < ActiveRecord::Migration
  def change
    create_table :games do |t|
      t.integer :player1_id, null: false
      t.integer :player2_id, null: false
      t.integer :currentplayer_id
      t.integer :winningplayer_id
      t.integer :losingplayer_id
      t.boolean :isfinished, :default => false

      t.timestamps
    end
  end
end

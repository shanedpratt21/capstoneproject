class AddPlayerDifficultyVotesToQuestions < ActiveRecord::Migration
  def change
    add_column :questions, :player_difficulty_votes, :integer, :default => 0
  end
end

class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.references :category, index: true
      t.string :username
      t.string :question
      t.integer :difficulty

      t.timestamps
    end
  end
end

class CreateChallenges < ActiveRecord::Migration
  def change
    create_table :challenges do |t|
      t.integer :game_id
      t.integer :challenging_id
      t.integer :challenged_id
      t.integer :art_question_id
      t.integer :science_question_id
      t.integer :sports_question_id
      t.integer :history_question_id
      t.integer :geography_question_id
      t.integer :entertainment_question_id
      t.integer :challenging_correct
      t.integer :challenged_correct

      t.timestamps
    end
  end
end

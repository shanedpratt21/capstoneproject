class AddTotalAndCategoryStatsToPlayerStats < ActiveRecord::Migration
  def change
    add_column :player_stats, :total_correct, :integer, :default => 0
    add_column :player_stats, :total_attempted, :integer, :default => 0
    add_column :player_stats, :art_correct, :integer, :default => 0
    add_column :player_stats, :art_attempted, :integer, :default => 0
    add_column :player_stats, :sports_correct, :integer, :default => 0
    add_column :player_stats, :sports_attempted, :integer, :default => 0
    add_column :player_stats, :entertainment_correct, :integer, :default => 0
    add_column :player_stats, :entertainment_attempted, :integer, :default => 0
    add_column :player_stats, :geography_correct, :integer, :default => 0
    add_column :player_stats, :geography_attempted, :integer, :default => 0
    add_column :player_stats, :science_correct, :integer, :default => 0
    add_column :player_stats, :science_attempted, :integer, :default => 0
    add_column :player_stats, :history_correct, :integer, :default => 0
    add_column :player_stats, :history_attempted, :integer, :default => 0
  end
end

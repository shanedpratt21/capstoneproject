class AddAchievementsToPlayerStats < ActiveRecord::Migration
  def change
    add_column :player_stats, :total_achievement, :boolean, :default => false
    add_column :player_stats, :art_achievement, :boolean, :default => false
    add_column :player_stats, :sports_achievement, :boolean, :default => false
    add_column :player_stats, :entertainment_achievement, :boolean, :default => false
    add_column :player_stats, :geography_achievement, :boolean, :default => false
    add_column :player_stats, :science_achievement, :boolean, :default => false
    add_column :player_stats, :history_achievement, :boolean, :default => false
  end
end

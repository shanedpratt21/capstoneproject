class ChangeLevelInPlayerStats < ActiveRecord::Migration
  def change
    change_column :player_stats, :level, :integer, :default => 1
  end
end

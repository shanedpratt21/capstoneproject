class AddSoundFlagToUsers < ActiveRecord::Migration
  def change
  	add_column :users, :sound_flag, :boolean, :default => false
  end
end

class AddPendingToChallenges < ActiveRecord::Migration
  def change
    add_column :challenges, :challenge_pending, :boolean
  end
end

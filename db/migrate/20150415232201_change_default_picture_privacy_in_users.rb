class ChangeDefaultPicturePrivacyInUsers < ActiveRecord::Migration
  def change
    change_column :users, :picture_privacy, :boolean, :default => false
  end
end

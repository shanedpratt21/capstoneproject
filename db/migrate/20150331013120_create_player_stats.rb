class CreatePlayerStats < ActiveRecord::Migration
  def change
    create_table :player_stats do |t|
      t.references :user, index: true
      t.integer :level, default: 0
      t.integer :wins, default: 0
      t.integer :losses, default: 0
      t.integer :resigns, default: 0

      t.timestamps
    end
  end
end

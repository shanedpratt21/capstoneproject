class RemoveDifficultyFromQuestions < ActiveRecord::Migration
  def change
    remove_column :questions, :difficulty
  end
end

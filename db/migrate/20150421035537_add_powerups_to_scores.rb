class AddPowerupsToScores < ActiveRecord::Migration
  def change
    add_column :scores, :retry, :boolean, :default => false
    add_column :scores, :reset_time, :boolean, :default => false
    add_column :scores, :stop_time, :boolean, :default => false
  end
end

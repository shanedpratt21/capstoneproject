class ChangeAccessLevelInUsers < ActiveRecord::Migration
  def change
    change_column :users, :access_level, :integer, :default => 0
  end
end

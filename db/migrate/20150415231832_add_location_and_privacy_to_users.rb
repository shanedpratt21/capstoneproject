class AddLocationAndPrivacyToUsers < ActiveRecord::Migration
  def change
    add_column :users, :location, :string
    add_column :users, :location_privacy, :boolean, :default => false
  end
end

class AddPicturePrivacyToUsers < ActiveRecord::Migration
  def change
    add_column :users, :picture_privacy, :boolean
  end
end

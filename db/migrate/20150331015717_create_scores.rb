class CreateScores < ActiveRecord::Migration
  def change
    create_table :scores do |t|
      t.references :user, index: true
      t.references :game, index: true
      t.integer :points, :default => 0
      t.integer :crown_progress, :default => 0
      t.boolean :sports_category, :default => false
      t.boolean :art_category, :default => false
      t.boolean :entertainment_category, :default => false
      t.boolean :history_category, :default => false
      t.boolean :geography_category, :default => false
      t.boolean :science_category, :default => false

      t.timestamps
    end
  end
end

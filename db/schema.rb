# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150421102710) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "answers", force: true do |t|
    t.integer  "question_id"
    t.boolean  "correct"
    t.text     "answer_text"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "answers", ["question_id"], name: "index_answers_on_question_id", using: :btree

  create_table "categories", force: true do |t|
    t.text     "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "challenges", force: true do |t|
    t.integer  "game_id"
    t.integer  "challenging_id"
    t.integer  "challenged_id"
    t.integer  "art_question_id"
    t.integer  "science_question_id"
    t.integer  "sports_question_id"
    t.integer  "history_question_id"
    t.integer  "geography_question_id"
    t.integer  "entertainment_question_id"
    t.integer  "challenging_correct"
    t.integer  "challenged_correct"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "challenging_category"
    t.integer  "challenged_category"
    t.boolean  "challenge_pending"
  end

  create_table "games", force: true do |t|
    t.integer  "player1_id",                       null: false
    t.integer  "player2_id",                       null: false
    t.integer  "currentplayer_id"
    t.integer  "winningplayer_id"
    t.integer  "losingplayer_id"
    t.boolean  "isfinished",       default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "monthly_player_stats", force: true do |t|
    t.integer  "user_id"
    t.integer  "total_correct"
    t.integer  "total_attempted"
    t.integer  "art_correct"
    t.integer  "art_attempted"
    t.integer  "sports_correct"
    t.integer  "sports_attempted"
    t.integer  "entertainment_correct"
    t.integer  "entertainment_attempted"
    t.integer  "geography_correct"
    t.integer  "geography_attempted"
    t.integer  "science_correct"
    t.integer  "science_attempted"
    t.integer  "history_correct"
    t.integer  "history_attempted"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "player_stats", force: true do |t|
    t.integer  "user_id"
    t.integer  "level",                     default: 1
    t.integer  "wins",                      default: 0
    t.integer  "losses",                    default: 0
    t.integer  "resigns",                   default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "total_correct",             default: 0
    t.integer  "total_attempted",           default: 0
    t.integer  "art_correct",               default: 0
    t.integer  "art_attempted",             default: 0
    t.integer  "sports_correct",            default: 0
    t.integer  "sports_attempted",          default: 0
    t.integer  "entertainment_correct",     default: 0
    t.integer  "entertainment_attempted",   default: 0
    t.integer  "geography_correct",         default: 0
    t.integer  "geography_attempted",       default: 0
    t.integer  "science_correct",           default: 0
    t.integer  "science_attempted",         default: 0
    t.integer  "history_correct",           default: 0
    t.integer  "history_attempted",         default: 0
    t.boolean  "total_achievement",         default: false
    t.boolean  "art_achievement",           default: false
    t.boolean  "sports_achievement",        default: false
    t.boolean  "entertainment_achievement", default: false
    t.boolean  "geography_achievement",     default: false
    t.boolean  "science_achievement",       default: false
    t.boolean  "history_achievement",       default: false
  end

  add_index "player_stats", ["user_id"], name: "index_player_stats_on_user_id", using: :btree

  create_table "questions", force: true do |t|
    t.string   "username"
    t.string   "question"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "category_id"
    t.boolean  "approved"
    t.integer  "level"
    t.integer  "player_difficulty_votes", default: 0
  end

  create_table "scores", force: true do |t|
    t.integer  "user_id"
    t.integer  "game_id"
    t.integer  "points",                 default: 0
    t.integer  "crown_progress",         default: 0
    t.boolean  "sports_category",        default: false
    t.boolean  "art_category",           default: false
    t.boolean  "entertainment_category", default: false
    t.boolean  "history_category",       default: false
    t.boolean  "geography_category",     default: false
    t.boolean  "science_category",       default: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "retry",                  default: false
    t.boolean  "reset_time",             default: false
    t.boolean  "stop_time",              default: false
  end

  add_index "scores", ["game_id"], name: "index_scores_on_game_id", using: :btree
  add_index "scores", ["user_id"], name: "index_scores_on_user_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "provider"
    t.string   "uid"
    t.string   "name"
    t.string   "oauth_token"
    t.datetime "oauth_expires_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.integer  "access_level",           default: 0
    t.integer  "difficulty_level"
    t.integer  "total_correct"
    t.boolean  "picture_privacy",        default: false
    t.string   "location"
    t.boolean  "location_privacy",       default: false
    t.integer  "coins",                  default: 10
    t.boolean  "sound_flag",             default: false
  end

  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
